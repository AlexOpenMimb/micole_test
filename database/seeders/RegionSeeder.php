<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Region;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RegionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for ($i=0; $i < count(Region::COLOMBIA) ; $i++) {
            Region::create([
                'name'        => Region::COLOMBIA[$i],
                'country_id' =>  Country::handleId('Colombia')->first()['id'],

            ]);
        }
        for ($i=0; $i < count(Region::VENEZUELA) ; $i++) {
            Region::create([
                'name'        => Region::VENEZUELA[$i],
                'country_id' =>  Country::handleId('Venezuela')->first()['id'],

            ]);
        }
        for ($i=0; $i < count(Region::ECUADOR) ; $i++) {
            Region::create([
                'name'        => Region::ECUADOR[$i],
                'country_id' =>  Country::handleId('Ecuador')->first()['id'],

            ]);
        }
    }
}
