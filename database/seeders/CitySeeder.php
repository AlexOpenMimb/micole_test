<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Region;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Ciudades de Colombia
        for ($i=0; $i < count(City::ATLANTICO) ; $i++) {
            City::create([
                'name'        => City::ATLANTICO[$i],
                'region_id' =>  Region::handleId('Atlantico')->first()['id'],

            ]);
        }
        for ($i=0; $i < count(City::BOLIVAR) ; $i++) {
            City::create([
                'name'        => City::BOLIVAR[$i],
                'region_id' =>  Region::handleId('Bolivar')->first()['id'],

            ]);
        }
        for ($i=0; $i < count(City::SUCRE) ; $i++) {
            City::create([
                'name'        => City::SUCRE[$i],
                'region_id' =>  Region::handleId('Sucre')->first()['id'],

            ]);
        }

        // Ciudades de Venezuela

        for ($i=0; $i < count(City::TACHIRA) ; $i++) {
            City::create([
                'name'        => City::TACHIRA[$i],
                'region_id' =>  Region::handleId('Táchira')->first()['id'],

            ]);
        }
        for ($i=0; $i < count(City::APURE) ; $i++) {
            City::create([
                'name'        => City::APURE[$i],
                'region_id' =>  Region::handleId('Apure')->first()['id'],

            ]);
        }
        for ($i=0; $i < count(City::MERIDA) ; $i++) {
            City::create([
                'name'        => City::MERIDA[$i],
                'region_id' =>  Region::handleId('Merida')->first()['id'],

            ]);
        }
        // Ciudades de Ecuador

        for ($i=0; $i < count(City::LOSRIOS) ; $i++) {
            City::create([
                'name'        => City::LOSRIOS[$i],
                'region_id' =>  Region::handleId('Los Rios')->first()['id'],

            ]);
        }
        for ($i=0; $i < count(City::GUAYAS) ; $i++) {
            City::create([
                'name'        => City::GUAYAS[$i],
                'region_id' =>  Region::handleId('Guayas')->first()['id'],

            ]);
        }
        for ($i=0; $i < count(City::SANTAELENA) ; $i++) {
            City::create([
                'name'        => City::SANTAELENA[$i],
                'region_id' =>  Region::handleId('Santa Elena')->first()['id'],

            ]);
        }
    }
}
