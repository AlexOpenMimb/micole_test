<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\Program;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
      $phone = fake()->phoneNumber();
        return [
            'name' => fake()->name(),
            'email'   => fake()->unique()->safeEmail(),
            'email-2' => fake()->unique()->safeEmail(),
            'password' => Hash::make(Str::random(8)),
            'postal' => fake()->randomNumber(5, true),
            'phone' => $phone,
            'website' => fake()->url(),
            'fax'   => $phone,
            'address' => fake()->streetAddress(),
            'latitude' => fake()->latitude($min = -90, $max = 90),
            'longitude'=> fake()->latitude($min = -90, $max = 90),
            'plan_preference' =>  Program::PLAN[array_rand(Program::PLAN,1)], 
            'business_status' => Program::BUSSINESSTATUS[array_rand(Program::BUSSINESSTATUS,1)],
            'google_rating' => fake()->randomDigit(),
            'revisor' => Program::REVISROR[array_rand(Program::REVISROR,1)],
            'status'  => Program::STATUS[array_rand(Program::STATUS,1)],
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'program_id' => Program::all()->random()->id,
            'city_id'    => City::all()->random()->id
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
    public function unverified()
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
