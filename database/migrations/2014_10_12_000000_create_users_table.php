<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

     // Para ejecutar los seeder y el factory ejecutar el comando php artisan user:factoy
    public function up()
    {

        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            
            $table->timestamps();
            $table->softDeletes();
        });



        Schema::create('regions', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->unsignedBigInteger('country_id');
            $table->foreign('country_id')->references('id')->on('countries');

            $table->timestamps();
            $table->softDeletes();
        });



        Schema::create('cities', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->unsignedBigInteger('region_id');
            $table->foreign('region_id')->references('id')->on('regions');

            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('programs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
      
            $table->timestamps();
            $table->softDeletes();
        });



        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('email-2')->unique();
            $table->string('password');
            $table->bigInteger('postal')->nullable();
            $table->string('phone')->nullable();
            $table->string('website')->nullable();
            $table->string('fax')->nullable();
            $table->string('address')->nullable();
            $table->float('latitude', 8, 6)->nullable();
            $table->float('longitude', 8, 6)->nullable();
            $table->string('plan_preference')->nullable();
            $table->string('business_status')->nullable();
            $table->float('google_rating', 2, 1)->nullable();
            $table->string('revisor')->nullable();
            $table->enum('status',['Activo','Inactivo']);
           
            
            $table->timestamp('email_verified_at')->nullable();

            $table->unsignedBigInteger('program_id');
            $table->foreign('program_id')->references('id')->on('programs');

            $table->unsignedBigInteger('city_id');
            $table->foreign('city_id')->references('id')->on('cities');



            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('country');
        Schema::dropIfExists('regions');
        Schema::dropIfExists('cities');
        Schema::dropIfExists('programs');
        Schema::dropIfExists('users');
    }
};
