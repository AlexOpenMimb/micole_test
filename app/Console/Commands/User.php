<?php

namespace App\Console\Commands;

use App\Models\User as ModelsUser;
use Database\Seeders\CitySeeder;
use Database\Seeders\CountrySeeder;
use Database\Seeders\ProgramSeeder;
use Database\Seeders\RegionSeeder;
use Illuminate\Console\Command;

class User extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:factory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ejecuta el factory del user';

    /**
     * Execute the console command.
     *
     * @return int
     */

    //  Manejador del comando
    
    public function handle()
    {
        $this->call(CountrySeeder::class);
        $this->call(RegionSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(ProgramSeeder::class);
        ModelsUser::factory(5)->create();
    }
}
