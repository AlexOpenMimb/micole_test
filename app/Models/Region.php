<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Region extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'regions';

    protected $fillable = [
        'name',
        'country_id'

    ];

    const COLOMBIA = [
        'Atlantico',
        'Sucre',
        'Bolivar'
    ];

    const VENEZUELA = [
        'Táchira',
        'Apure',
        'Merida'
    ];
    const ECUADOR = [

        'Los Rios',
        'Guayas',
        'Santa Elena'
    ];


    // Relaciones
    public function country()
    {
        return $this->BelongsTo(Country::class);
    }

    public function city()
    {
        return $this->hasMany(City::class);
    }



    // Scope para obtener Id

    public function scopeHandleId($query, $name)
    {
        return $query->where('name', $name);
    }


}
