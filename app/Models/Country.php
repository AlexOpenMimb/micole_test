<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'countries';

    protected $fillable = [
        'name'

    ];

    const COUNTRY = [
        'Colombia',
        'Venezuela',
        'Ecuador'
    ];


    // Relaciones 
    public function region()
    {
        return $this->hasMany(Region::class);
    }

    // Scope para obtener Id

    public function scopeHandleId($query, $name)
    {
        return $query->where('name', $name);
    }

}
