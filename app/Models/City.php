<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'cities';

    protected $fillable = [
        'name',
        'region_id'
    ];
    // Ciudades de Colombia
    const ATLANTICO = [
        'Barranquilla',
        'Soledad',
        'Galapa'
    ];
    const BOLIVAR = [
        'Cartagena',
        'Bayunca',
        'Turbaco'
    ];
    const SUCRE = [
        'Sincelejo',
        'Sampues',
        'Corzal'
    ];

    // Ciudades de Venezuela
    const TACHIRA = [
        'San Cristobal',
        'San Juan De Colón',
        'La Grita'
    ];
    const APURE = [
        'La Victoria',
        'El Nula',
        'Elorza'
    ];

    const MERIDA = [
        'Arapuey',
        'Batatal',
        'Guarape'
    ];

    // Ciudades de Ecuador

    const LOSRIOS = [
        'Baba',
        'Mocache',
        'Palenque'
    ];

    const GUAYAS = [
        'Guayaquil',
        'Milagro',
        'Colimes'
    ];
    
    const SANTAELENA = [
        'Salinas',
        'Libertad',
        'Santa Elena'
    ];


    // Relaciones
    public function region() 
    {
        return $this->belongsTo(Region::class);
    }


}
