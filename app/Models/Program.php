<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Program extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'programs';

    protected $fillable = [
        'name'
    ];


    const PROGRAM = [
        'Ingeniería de Sistemas',
        'Derecho',
        'Medicina',
        'Psicología',
        'Diseño'
    ];


    const PLAN = [
        'Anual',
        'Semestral',
        'Trimestral'
    ];

    const BUSSINESSTATUS = [
        'Vacaciones',
        'Operacionala',
        'Suspendido',
        'Aplazado'
    ];

    const REVISROR = [
        'Luis Castro',
        'Carlos Hernandez',
        'Daniel Samper',
        'Juan Santos',
        'Carmen Bolaños',
        'Andres Hurtado'
    ];

    const STATUS = [
        'Activo',
        'Inactivo'
    ];

    // Relaciones

    public function user()
    {
        return $this->hasMany(User::class);
    }
}
